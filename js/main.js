/*
- Написати функцію `filterBy()`, яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
- Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].
*/

// Реалізація задопомогою класу

class Filter {
    constructor(array, type){
        this.array=array;
        this.type=type;
    }
    filterBy(){
      let res=(this.array.filter(el=> typeof el !== this.type))
      document.getElementById("Array").innerHTML  += `<p>Початковий масив: ${this.array}</p>`
      document.getElementById("Array").innerHTML  += `<br>`
      document.getElementById("Array").innerHTML  += `<p>Умова: не виводити елементи масиву які мають тип даних "${this.type}"</p>`
      document.getElementById("Array").innerHTML  += `<br>`
      document.getElementById("Array").innerHTML  += `<p>Масив з врахуванням умови: ${res}</p>`
      document.getElementById("Array").innerHTML  += `<hr class="line">`


    }

}


const filter1=new Filter(['двері', '3', 1, 'вікно', 'Олексі', 150], "string")
const filter2=new Filter(['3', 33, 1, '1444', '-12', -111, 'вода', 'number'], "number")
const filter3=new Filter(['Auto', '99', 'null',  'work', 150], "number")


filter1.filterBy()
filter2.filterBy()
filter3.filterBy()


// Реалізація за допомогою функції

function filterBy2 (array, type){
    return array.filter(el=> typeof el !== type)
}
console.log(filterBy2(['двері', '3', 1, 'вікно', 'Олексі',150],"string"))
console.log(filterBy2(['3', 33, 1, '1444', '-12', -111, 'вода', 'number'], "number"))
console.log(filterBy2(['Auto', '99', 'null',  'work', 150], "number"))
